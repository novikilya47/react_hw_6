import {EDIT_USER, FETCH_USERS_SUCCESS} from "../Constants/constants";
const defaultState = {}

export default function reducer (state = defaultState, action){
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                email: action.data.email,
            }
            return newState;
        case FETCH_USERS_SUCCESS:
            let nState = {
                name: action.data.name,
                email: action.data.email,
            };    
        return nState;
        default:
            return state;
    }
};
