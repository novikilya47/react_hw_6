import { AUTHENTICATE } from "../Constants/constants";

export const defaultState = {isAuth: false};

export default function reducer (state = defaultState, action) {
    switch (action.type) {
        case AUTHENTICATE: return {isAuth: !state.isAuth}
        default:
            return state;
    }
};
