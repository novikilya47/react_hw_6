import {createStore, combineReducers, applyMiddleware} from "redux";
import profileReducer from '../Reducers/Profile.reducer';
import productReducer from '../Reducers/Product.reducer';
import authReducer from '../Reducers/Auth.reducer'
import thunk from "redux-thunk";

export const rootReducer = combineReducers({
    profile: profileReducer,
    product: productReducer,
    auth: authReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
