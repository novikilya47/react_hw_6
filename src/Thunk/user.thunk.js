import { getUsersQuery } from "../Services/profile.service";
import { fetchUsersSuccess } from "../Actions/Actions";

export const getUsers = () => (dispatch) => {
    getUsersQuery()
        .then((response) => response.json())
        .then((data) => {
            dispatch(fetchUsersSuccess(data))
        })
        .catch((err) => {
            console.log(err)
        })
};
