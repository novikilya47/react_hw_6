import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";
import {editUser} from "../../Actions/Actions";
import {getUsers} from "../../Thunk/user.thunk"

function Profile(props) {
    const [name, setName] = useState(props.profile.name);
    const [email, setEmail] = useState(props.profile.email);

    useEffect(() => {
        props.getUsers();
    }, []);

    const handleEdit = () => {
        let user = {
            name: name ? name : props.profile.name,
            email: email ? email : props.profile.email
        };
        props.editUser(user);
    }
    
    return (
        <div className="Info">
            <div>Имя: {props.profile.name}</div>
            <div>Email: {props.profile.email}</div>
            <div className="Info_inputs">
                <div className="Info_input"><p>Имя</p><textarea type='text' onChange={(e) => setName(e.target.value)}/></div>
                <div className="Info_input"><p>Email</p><textarea type='text' onChange={(e) => setEmail(e.target.value)}/>  </div>
            </div>  
            <button onClick={() => handleEdit()}>Изменить информацию</button>   
        </div>
    )
}

const mapStateToProps = (state) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => {dispatch(editUser(data))},
    getUsers:()=> dispatch(getUsers()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
