import React from 'react';
import { connect } from "react-redux";
import { Route } from 'react-router-dom';

const Login  = () => {
    return (
        <div className="Info">
             <div className="Info_inputs">
                <div className="Info_input"><p>Имя</p><textarea type='text' onChange={(e) => setName(e.target.value)}/></div>
                <div className="Info_input"><p>Email</p><textarea type='text' onChange={(e) => setEmail(e.target.value)}/>  </div>
            </div> 
        </div>
    )
}

const HOC = ({component: Component, auth, ...rest}) =>(
    <Route
        {...rest}
        render={props => {
            return(
                auth.isAuth ? (<Component {...props}/>) : (<Login/>)
            )
        }}
    />
)

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps)(HOC)
