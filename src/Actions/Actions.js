import {FIND_PRODUCT, ADD_PRODUCT, EDIT_PRODUCT, DELETE_PRODUCT, EDIT_USER, FETCH_USERS_SUCCESS, RESET_PRODUCT, AUTHENTICATE} from "../Constants/constants"

export const findProduct = (data) => ({type : FIND_PRODUCT, data : data});
export const resetState = () => ({type : RESET_PRODUCT});
export const addProduct = (data) => ({type : ADD_PRODUCT, data});
export const editProduct = (data) => ({type : EDIT_PRODUCT, data});
export const deleteProduct = (id) => ({type : DELETE_PRODUCT, id});
export const editUser = (data) => ({type : EDIT_USER, data});
export const fetchUsersSuccess = (data) => ({type : FETCH_USERS_SUCCESS, data});
export const authenticate = () => ({type : AUTHENTICATE});